package main

import (
	"errors"
	"flag"
	"fmt"

	"sridharmane.com/media_extractor/hello/metadata"
	"sridharmane.com/media_extractor/hello/scraper"
)

var (
	// ErrNilChecker returned if Check invoked on a nil checker
	ErrNilChecker = errors.New("attempted Check with nil Checker")

	// ErrNilLogger returned if the Check function is provide a nil logger
	ErrNilLogger = errors.New("nil logger provided for Check")
)

func main() {
	fmt.Println("Media Extractor")
	rootDir := flag.String("root", "~/Music", "a string")
	outputDir := flag.String("out", "~/media_converter/output.json", "a string")
	maxCount := flag.Int("max", 10, "an int")
	flag.Parse()

	// rootPath := os.Args[1]
	// outputPath := os.Args[2]

	if *rootDir == "" || *outputDir == "" {
		panic("[root] and [out] flags cannot be empty.")
	}
	// for i, path := range scraper.Scrape(rootPath) {
	// 	fmt.Println(i, ":", path)
	// }

	paths := scraper.Scrape(*rootDir)
	plistPaths := metadata.Extract(paths[0:*maxCount])
	jsonPaths := metadata.ConvertToJson(plistPaths[0:*maxCount])
	combinePath := metadata.CombineJson(jsonPaths[0:*maxCount])
	metadata.CleanJson(combinePath)
	metadata.MoveOutput(combinePath, *outputDir)
	fmt.Println("converted", len(jsonPaths), *outputDir)
}
