# Media MetaData Extractor
This tool scrapes your audio files from iPod or or iTunes library and generates 
a JSON list of files with fields:
```
[{
    "title": "",
    "displayName: "",
    "album": "",
    "trackNumber": 0,
    "artists": [""],
    "alternateNames": [""],
    "durationSeconds": 0,
    "originalFileName": "",
    "kind": "",
}]
```
