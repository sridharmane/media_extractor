package metadata

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	"sridharmane.com/media_extractor/hello/utils"
)

var (
	keyMap = map[string]string{
		"kMDItemTitle":            "title",
		"kMDItemDisplayName":      "displayName",
		"kMDItemAlbum":            "album",
		"kMDItemAudioTrackNumber": "trackNumber",
		"kMDItemAuthors":          "artists",
		"kMDItemAlternateNames":   "alternateNames",
		"kMDItemDurationSeconds":  "durationSeconds",
		"kMDItemFSName":           "originalFileName",
		"kMDItemKind":             "kind",
	}
)

func Extract(paths []string) []string {
	var newPaths []string

	var keyArgs []string

	for key := range keyMap {
		keyArgs = append(keyArgs, "-name", key)
	}

	for _, file := range paths {
		var tempDir = os.TempDir() + "metadata/plist/"
		var ext = ".plist"
		var name = utils.FileNameWithoutExtension(file)
		createDir := exec.Command("mkdir", "-p", tempDir)
		createDir.Output()
		var newPath = tempDir + name + ext

		var args []string
		args = append(args, "-plist")
		args = append(args, newPath)
		args = append(args, file)

		// cmd := exec.Command("mdls", "-plist", newPath, args... , file)
		cmd := exec.Command("mdls", append(keyArgs, args...)...)
		_, err := cmd.Output()
		if err == nil {
			newPaths = append(newPaths, newPath)
		}
	}
	return newPaths
}

func ConvertToJson(paths []string) []string {
	var newPaths []string

	for _, file := range paths {
		var tempDir = os.TempDir() + "metadata/json/"
		var ext = ".json"
		var name = utils.FileNameWithoutExtension(file)
		createDir := exec.Command("mkdir", "-p", tempDir)
		createDir.Output()
		var newPath = tempDir + name + ext

		cmd := exec.Command("plutil", "-convert", "json", "-o", newPath, file)
		_, err := cmd.Output()
		if err == nil {
			newPaths = append(newPaths, newPath)
		} else {
			fmt.Println(err)
		}
	}
	return newPaths

}

func CombineJson(paths []string) string {
	var path string = os.TempDir() + "metadata/combined.json"
	combinedFile, err := os.Create(path)
	if err != nil {
		panic(err)
		return path
	}
	var content string = "["
	for i, file := range paths {
		data, err := ioutil.ReadFile(file)
		if err != nil {
			panic(err)
			break
		}
		content += string(data)
		if i < len(paths)-1 {
			content += ","
		}
	}
	content += "]"
	combinedFile.WriteString(content)
	combinedFile.Close()
	return path
}

func CleanJson(path string) string {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		panic(err)
	}
	var contentStr = string(data)
	for k, v := range keyMap {
		contentStr = strings.Replace(contentStr, k, v, -1)
	}
	ioutil.WriteFile(path, []byte(contentStr), os.ModeTemporary)
	return path
}

func MoveOutput(fromPath string, toPath string) {
	mkdir := exec.Command("mkdir", "-p", filepath.Dir(toPath))
	mkdir.Output()
	mv := exec.Command("mv", fromPath, toPath)
	mv.Output()
}
