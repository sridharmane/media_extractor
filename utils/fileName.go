package utils

import (
	"path/filepath"
	"strings"
)

func FileNameWithoutExtension(fullName string) string {
	var base = filepath.Base(fullName)
	return base[0:strings.Index(base, ".")]
}
