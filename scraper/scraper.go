package scraper

import (
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
)

func Scrape(dir string) []string {

	var acceptedFileTypes = [...]string{".mp3", ".m4a", ".flac"}

	var list []string

	err := filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {

		if info.IsDir() {
			files, err := ioutil.ReadDir(path)
			for _, f := range files {
				for _, ext := range acceptedFileTypes {

					if filepath.Ext(f.Name()) == ext {
						list = append(list, path+"/"+f.Name())
						break
					}
				}

				// fmt.Println(f.Name())
			}

			if err != nil {
				log.Fatal(err)
			}
		}
		// list = append(list, info.Name)
		return nil
	})
	if err != nil {
		log.Fatal(err)
	}

	return list
}
